#include <iostream>
#include "classMath.h"

bool areEqual(Math* left, Math* right) {
	if (left->a != right->a) return false;
	if (left->b != right->b) return false;
	if (left->c != right->c) return false;
	if (left->d != right->d) return false;
	return true;
}
bool func2equal(Math left, Math right) {
	double m[2], n[2];
	left.PolySecond(m); right.PolySecond(n);
	if (m[0] == n[0] && m[1] == n[1]) return true;
	else return false;
}
bool func3equal(Math left, Math right) {
	double m[3], n[3];
	left.PolyThird(m); right.PolyThird(n);
	if (m[0] == n[0] && n[1] == m[1] && m[2] == n[2]) return true;
	else return false;
}
bool func4equal(Math a, Math b) {
	//a - step
	//b - size
	//c - first element
	if (a.progressionA() == b.progressionA()) return true;
	else return false;
}
bool func5equal(Math a, Math b) {
	//a - step
	//b - size
	//c - first element
	if (a.progressionB() == b.progressionB()) return true;
	else return false;
}
bool func6equal(Math a, Math b) {
	//a - step
	//b - size
	//c - first element
	//d - num of element
	if (a.progressionI() == b.progressionI()) return true;
	else return false;
}