#pragma once
#include <math.h>
#define M_PI           3.14159265358979323846

class Math {
public:
	int a, b, c, d;


	Math() {
		a = 0; b = 0; c = 0; d = 0;
	}

	Math(int a1, int b1, int c1, int d1) {
		a = a1; b = b1; c = c1; d = d1;
	}
	Math(int a1, int b1, int c1) {
		a = a1; b = b1; c = c1; d = 0;
	}
	Math(int a1, int b1) {
		a = a1; b = b1; c = 0; d = 0;
	}
	Math(int a1) {
		a = a1; b = 0; c = 0; d = 0;
	}

	//functions


	double PolyFirst() {//ax + b = 0
		return -(double)b / a;
	}

	int PolySecond(double* ans) {//ax^2 + bx + c = 0

		if (a == 0 && b == 0 && c == 0) {
			return 1;
		}
		if (a == 0) {
			return 1;
		}

		int d;
		d = b * b - 4 * a*c;

		if (d < 0) {
			return 1;
		}

		if (d > -1) {
			ans[0] = (-b + sqrt(d)) / (2 * a);
			ans[1] = (-b - sqrt(d)) / (2 * a);
			return 2;
		}
		//else return 1;
	}

	void PolyThird(double *x) { // solve cubic equation x^3 + a*x^2 + b*x + c = 0
		double a1 = b / a,
			b1 = c / a,
			c1 = d / a;

		double a2 = a1*a1;
		double q = (a2 - 3 * b1) / 9;
		double r = (a1*(2 * a2 - 9 * b1) + 27 * c1) / 54;
		// equation x^3 + q*x + r = 0
		double r2 = r*r;
		double q3 = q*q*q;
		double A, B;
		if (r2 < q3) {
			double t = r / sqrt(q3);
			if (t < -1) t = -1;
			if (t > 1) t = 1;
			t = acos(t);
			a1 /= 3; q = -2 * sqrt(q);
			x[0] = q*cos(t / 3) - a1;
			x[1] = q*cos((t + 2 * M_PI) / 3) - a1;
			x[2] = q*cos((t - 2 * M_PI) / 3) - a1;
		}
		else {
			A = -pow(fabs(r) + sqrt(r2 - q3), 1. / 3);
			//A = -root3(fabs(r) + sqrt(r2 - q3));
			if (r < 0) A = -A;
			B = A == 0 ? 0 : B = q / A;

			a1 /= 3;
			x[0] = (A + B) - a1;
			x[1] = -0.5*(A + B) - a1;
			x[2] = 0.5*sqrt(3.)*(A - B);
		}

	}

	long int progressionA() {
		//a - step
		//b - size
		//c - first element
		long int *x = new long int[b];
		x[0] = c;
		for (int i = 1; i < b; i++) {
			x[i] = x[i - 1] + a;
		}
		long int summ = 0;
		for (int i = 0; i < b; i++) summ += x[i];
		return summ;
	}

	long int progressionB() {
		long int *x = new long int[b];
		x[0] = c;
		for (int i = 1; i < b; i++) {
			x[i] = x[i - 1] * a;
		}
		long int summ = 0;
		for (int i = 0; i < b; i++) summ += x[i];
		return summ;
	}

	long int progressionI() {
		//a - step
		//b - size
		//c - first element
		//d - num of element
		if (d - 1 > b) return -1;
		long int *x = new long int[b];
		x[0] = c;
		for (int i = 1; i < b; i++) {
			x[i] = x[i - 1] + a;
		}
		return x[d - 1];

	}
};