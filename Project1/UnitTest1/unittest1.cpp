#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Project1/Math.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(areEqual_true)
		{
			Math* a = new Math, *b = new Math;
			a->a = 1; a->b = 2; a->c = 3; a->d = 4;
			b->a = 1; b->b = 2; b->c = 3; b->d = 4;
			Assert::AreEqual(true, areEqual(a, b));
		}
		TEST_METHOD(areEqual_false)
		{
			Math* a = new Math, *b = new Math;
			a->a = 1; a->b = 2; a->c = 3; a->d = 4;
			b->a = 0; b->b = 2; b->c = 3; b->d = 4;
			Assert::AreNotEqual(true, areEqual(a, b));
		}
		

	};
	TEST_CLASS(function1)
	{
	public:

		TEST_METHOD(poly_one_true)
		{
			Math a(1, 2), b(1, 2);
			
			Assert::AreEqual(true, a.PolyFirst() == b.PolyFirst());
		}
		TEST_METHOD(poly_one_false)
		{
			Math a(1, 2), b(2, 1);
			Assert::AreNotEqual(true, a.PolyFirst() == b.PolyFirst());
		}


	};
	TEST_CLASS(function2)
	{
	public:

		TEST_METHOD(poly_two_true)
		{
			Math a(1, 1, -2), b(1, 1, -2);

			Assert::AreEqual(true, func2equal(a,b));
		}
		TEST_METHOD(poly_two_false)
		{
			Math a(1, 4, 4), b(1, 3, -4);
			Assert::AreNotEqual(true, func2equal(a,b));
		}
		TEST_METHOD(poly_two_invalid) {
			Math a(1, 1, -2); 
			double x[2];
			if (a.PolySecond(x) == 1) Assert::Fail();
		}
	};
	TEST_CLASS(function3)
	{
	public:

		TEST_METHOD(poly_three_true)
		{
			Math a(1, 1, -1, -1), b(1, 1, -1, -1);
			Assert::AreEqual(true, func3equal(a, b));
		}
		TEST_METHOD(poly_three_false)
		{
			Math a(1, 1, -1, -1), b(1, 2, 3, 4);
			Assert::AreNotEqual(true, func3equal(a, b));
		}
	};
	TEST_CLASS(function4)
	{
	public:

		TEST_METHOD(prograssionA_true)
		{
			//a - step
			//b - size
			//c - first element
			Math a(1, 2, 3), b(1, 2, 3);
			
			Assert::AreEqual(true, func4equal(a,b));
		}
		TEST_METHOD(progressionA_false)
		{
			Math a(1, 1, -1), b(1, 2, 3);
			Assert::AreNotEqual(true, func4equal(a, b));
		}
	};
	TEST_CLASS(function5)
	{
	public:

		TEST_METHOD(prograssionB_true)
		{
			//a - step
			//b - size
			//c - first element
			Math a(1, 2, 3), b(1, 2, 3);

			Assert::AreEqual(true, func5equal(a, b));
		}
		TEST_METHOD(progressionB_false)
		{
			Math a(1, 1, -1), b(1, 2, 3);
			Assert::AreNotEqual(true, func5equal(a, b));
		}
	}; 
	TEST_CLASS(function6)
	{
	public:

		TEST_METHOD(prograssionI_true)
		{
			//a - step
			//b - size
			//c - first element
			//d - num of element
			Math a(1, 2, 3, 1), b(1, 2, 3, 1);

			Assert::AreEqual(true, func6equal(a, b));
		}
		TEST_METHOD(progressionI_false)
		{
			//a - step
			//b - size
			//c - first element
			//d - num of element
			Math a(1, 2, 3, 2), b(1, 2, 3, 1);
			Assert::AreNotEqual(true, func6equal(a, b));
		}
		TEST_METHOD(prograssionI_invalid) {
			//a - step
			//b - size
			//c - first element
			//d - num of element
			Math a(1, 2, 3, 2);
			if (a.progressionI()== -1) Assert::Fail();
		}
	};
}